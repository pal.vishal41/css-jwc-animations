import React from 'react';
import styled from 'styled-components/macro';

import { WEIGHTS } from '../../constants';

const Sidebar = () => {
  return (
    <Wrapper>
      <Link href="/lifestyle">Lifestyle</Link>
      <Link href="/jordan">Jordan</Link>
      <ActiveLink href="/running">Running</ActiveLink>
      <Link href="/basketball">Basketball</Link>
      <Link href="/training">Training &amp; Gym</Link>
      <Link href="/football">Football</Link>
      <Link href="/skateboarding">Skateboarding</Link>
      <Link href="/us-football">American Football</Link>
      <Link href="/baseball">Baseball</Link>
      <Link href="/golf">Golf</Link>
      <Link href="/tennis">Tennis</Link>
      <Link href="/athletics">Athletics</Link>
      <Link href="/walking">Walking</Link>
    </Wrapper>
  );
};

const Wrapper = styled.aside``;

const Link = styled.a`
  display: block;
  text-decoration: none;
  font-weight: ${WEIGHTS.medium};
  color: var(--color-gray-900);
  line-height: 2;
  position: relative;
  margin-bottom: 16px;
  padding: 4px 10px;
  overflow: hidden;

  &::before {
    content: '';
    height: 350%;
    width: 2px;
    background: var(--color-gray-300);
    position: absolute;
    bottom: 3px;
    left: 0;
  }

  @media (prefers-reduced-motion: no-preference) {
    &::before {
      transition: transform 250ms ease-out, width 250ms ease-out;
      transform-origin: bottom;
    }
    &:hover {
      &::before {
        width: 3px;
        transform: rotate(90deg);
      }
    }
  }
`;

const ActiveLink = styled(Link)`
  color: var(--color-primary);
`;

export default Sidebar;
