import React from 'react';
import styled from 'styled-components/macro';

import { COLORS, QUERIES, WEIGHTS } from '../../constants';
import Logo from '../Logo';
import Icon from '../Icon';
import UnstyledButton from '../UnstyledButton';
import SuperHeader from '../SuperHeader';
import MobileMenu from '../MobileMenu';
import VisuallyHidden from '../VisuallyHidden';

const Header = () => {
  const [showMobileMenu, setShowMobileMenu] = React.useState(false);

  return (
    <header>
      <SuperHeader />
      <MainHeader>
        <LogoWrapper>
          <Logo />
        </LogoWrapper>
        <DesktopNav>
          <NavLinkWrapper>
            <NavLinkSecondary href="/sale">Sale</NavLinkSecondary>
            <NavLink href="/sale">Sale</NavLink>
          </NavLinkWrapper>
          <NavLinkWrapper>
            <NavLinkSecondary href="/new">New&nbsp;Releases</NavLinkSecondary>
            <NavLink href="/new">New&nbsp;Releases</NavLink>
          </NavLinkWrapper>
          <NavLinkWrapper>
            <NavLinkSecondary href="/men">Men</NavLinkSecondary>
            <NavLink href="/men">Men</NavLink>
          </NavLinkWrapper>
          <NavLinkWrapper>
            <NavLinkSecondary href="/women">Women</NavLinkSecondary>
            <NavLink href="/women">Women</NavLink>
          </NavLinkWrapper>          
          <NavLinkWrapper>
            <NavLinkSecondary href="/kids">Kids</NavLinkSecondary>
            <NavLink href="/kids">Kids</NavLink>
          </NavLinkWrapper>
          <NavLinkWrapper>
            <NavLinkSecondary href="/collections">Collections</NavLinkSecondary>
            <NavLink href="/collections">Collections</NavLink>
          </NavLinkWrapper>
        </DesktopNav>
        <MobileActions>
          <ShoppingBagButton>
            <Icon id="shopping-bag" />
            <VisuallyHidden>Open cart</VisuallyHidden>
          </ShoppingBagButton>
          <UnstyledButton>
            <Icon id="search" />
            <VisuallyHidden>Search</VisuallyHidden>
          </UnstyledButton>
          <UnstyledButton onClick={() => setShowMobileMenu(true)}>
            <Icon id="menu" />
            <VisuallyHidden>Open menu</VisuallyHidden>
          </UnstyledButton>
        </MobileActions>
        <Filler />
      </MainHeader>

      <MobileMenu
        isOpen={showMobileMenu}
        onDismiss={() => setShowMobileMenu(false)}
      />
    </header>
  );
};

const MainHeader = styled.div`
  display: flex;
  align-items: baseline;
  padding: 18px 32px;
  border-bottom: 1px solid var(--color-gray-300);
  overflow: auto;

  @media ${QUERIES.tabletAndSmaller} {
    justify-content: space-between;
    align-items: center;
    border-top: 4px solid var(--color-gray-900);
  }

  @media ${QUERIES.phoneAndSmaller} {
    padding-left: 16px;
    padding-right: 16px;
  }
`;

const DesktopNav = styled.nav`
  display: flex;
  gap: clamp(1rem, 9.2vw - 4.5rem, 3.5rem);
  margin: 0px 48px;

  @media ${QUERIES.tabletAndSmaller} {
    display: none;
  }
`;

const MobileActions = styled.div`
  display: none;

  @media ${QUERIES.tabletAndSmaller} {
    gap: 32px;
    display: flex;
  }

  @media ${QUERIES.phoneAndSmaller} {
    gap: 16px;
  }
`;

const LogoWrapper = styled.div`
  flex: 1;

  @media ${QUERIES.tabletAndSmaller} {
    flex: revert;
  }
`;

const ShoppingBagButton = styled(UnstyledButton)`
  transform: translateX(-2px);
`;

const Filler = styled.div`
  flex: 1;

  @media ${QUERIES.tabletAndSmaller} {
    display: none;
  }
`;

const NavLinkWrapper = styled.div`
  color: var(--color-gray-900);
  position: relative;
  perspective: 300px;
  transform-style: preserve-3d;
  --transition: transform 500ms ease-in-out;
  --transform: rotateX(270deg);
  
  &:first-of-type {
    color: var(--color-secondary);
  }

  &:hover {
    --transform: rotateX(10deg);
    --transition: transform 400ms ease-in-out;
  }
`;

const NavLink = styled.a`
  display: block;
  font-size: 1.125rem;
  text-transform: uppercase;
  text-decoration: none;
  color: inherit;
  font-weight: ${WEIGHTS.medium};
  position: relative;
  background-color: white;
  padding: 0 4px;
`;

const NavLinkSecondary = styled(NavLink)`
  font-weight: ${WEIGHTS.bold};
  position: absolute;
  inset: 0;
  background-color: var(--color-gray-700);
  color: hsl(${COLORS.white});
  @media (prefers-reduced-motion: no-preference) {
    transform-origin: top center;
    transition: var(--transition);
    transform: var(--transform);
  }
`;

export default Header;
